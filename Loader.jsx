import React from "react";

const Loader = () => {
  return (
    <div style={{marginTop:"10rem" }}>
      <i style={{ fontSize: '1000%' , color:'white' }} className="fa-brands fa-trello fa-bounce fa-2xl"></i>
    </div>
  );
};

export default Loader;
