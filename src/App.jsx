import "./App.css";
import Header from "./components/Header";
import HomePage from "./components/HomePage";
import { Container } from "@mui/material";
import { Route, Routes } from "react-router-dom";
import GetLists from "./components/ListManipulation/GetLists";

function App() {
  return (
    <>
      <div
      >
        <Header />
        <main>
          <div>
            <Container
              sx={{
                display: "flex",
                flexWrap: "wrap",
                gap: 5,
                paddingY: 5,
                justifyContent: "center",
              }}
            >
              <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/boards/:id" element={<GetLists />} />
              </Routes>
            </Container>
          </div>
        </main>
      </div>
    </>
  );
}

export default App;
