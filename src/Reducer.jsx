import { useReducer  } from "react";

const initialState = {
  data: [],
};

const reducer = (state, action) => {
  switch (action.type) {
    case "SET_DATA":
      return { ...state, data: action.payload };
    case "ADD_DATA":
      return { ...state, data: [...state.data, action.payload] };
    case "DELETE_DATA":
      return { ...state, data: [...state.data, action.payload] };
    default:
      return state;
  }
};

const useReducerFunction = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const setData = (data) => {
    dispatch({ type: "SET_DATA", payload: data });
  };

  const addData = (data) => {
    dispatch({ type: "ADD_DATA", payload: data });
  };

  return {
    state,
    setData,
    addData,
  };
};

export default useReducerFunction;
