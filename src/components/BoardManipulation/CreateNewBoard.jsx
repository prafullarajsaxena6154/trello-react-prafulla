import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Alert, Button, CardActionArea, TextField } from "@mui/material";
import axios from "axios";
import { Mode } from "../Theme";
import { useContext } from "react";
import { useLoginContext } from "../LoginDetails";
export default function CreateNewBoard({ noOfBoards, boards }) {
  const [addCard, setAddCard] = React.useState(false);
  const { apiToken,apiKey } = useLoginContext();
  const [boardName, setBoardName] = React.useState("");
  const [error, setError] = React.useState("");
  const { darkMode } = useContext(Mode);

  const styles = {
    paperContainer: {
      marginBottom: 30,
      color: darkMode ? "white" : "black",
      height: 203,
      width: 300,
      backgroundSize: "cover",
      background: !darkMode?"white":"black",
      display: "flex",
      justifyContent: "center",
      backgroundPosition: "center",
      alignItems: "center",
    },
  };
  noOfBoards = 10 - noOfBoards;
  function addNewBoard() {
    if (noOfBoards != 0) {
      setError("");
      setAddCard(true);
    }
  }
  function createBoard(boardName) {
    if (boardName != "") {
      axios
        .post(
          `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => {
          setError("");
          setBoardName("");
          return response.data;
        })
        .then((data) => {
          boards(data);
        })
        .catch((err) => {
          setError("Error! Please try again!");
        });
    } else {
      setError("Enter Name for board!");
    }
  }

  return !addCard ? (
    <Card elevation={20} sx={styles.paperContainer} onClick={addNewBoard}>
      <CardActionArea>
        <CardContent
          sx={{ display: "flex", flexDirection: "column", paddingY: 5.2 }}
          className={darkMode ? "dark" : "null"}
        >
          <Typography sx={{ fontSize: 30, textAlign: "center" }}>
            Create New Board
          </Typography>
          <Typography sx={{ fontSize: 30, textAlign: "center" }}>
            <i className="fa-solid fa-circle-plus fa-beat"></i>
          </Typography>
          <Typography sx={{ fontSize: 20, textAlign: "center" }}>
            {noOfBoards} Remaining
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  ) : (
    <Card elevation={10} sx={styles.paperContainer}>
      <CardActionArea>
        <CardContent sx={{ display: "flex", flexDirection: "column", gap: 3 }}>
          <TextField
            label="New Board Name"
            onChange={(event) => {
              setBoardName(event.target.value);
            }}
            variant="outlined"
            sx={{ bgcolor: "white", borderRadius: 1, border: "none" }}
            value={boardName}
          ></TextField>
          {error && <Alert severity="error">{error}</Alert>}
          <div
            className="buttonsDiv"
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <Button
              variant="outlined"
              onClick={() => {
                createBoard(boardName);
              }}
            >
              Create
            </Button>
            <Button onClick={() => setAddCard(false)} variant="outlined">
              Cancel
            </Button>
          </div>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
