import React from "react";
import { Card, CardActionArea, CardContent } from "@mui/material";
import { Link } from "react-router-dom";
import Delete from "../Delete";
import { Mode } from "../Theme";
import { useContext } from "react";

const DisplayBoard = ({ board, initialData, allData }) => {
  const { darkMode } = useContext(Mode);
  let boardId = board.id;
  let boardName = board.name;
  let eleName = "Board";
  let boardBG =
    board.prefs.backgroundImage === null
      ? "https://thumbs.dreamstime.com/blog/2021/02/how-to-create-stunning-images-ordinary-sceneries-landscape-photography-36963-image187168069.jpg"
      : board.prefs.backgroundImage;
  const styles = {
    paperContainer: {
      color: !darkMode?"white":"black" ,
      height: 150,
      width: 300,
      backgroundSize: "cover",
      backgroundImage: `url(${boardBG})`,
      display: "flex",
      justifyContent: "center",
      backgroundPosition: "center",
      alignItems: "center",
      fontSize: 40,
    },
  };

  return (
    <>
      <Card elevation={10} sx={{ height: 203 , bgcolor:!darkMode?"white":"black" }} >
        <Link to={`/boards/${boardId}`}>
          <CardActionArea style={styles.paperContainer} >
            <CardContent
              sx={{
                display: "flex",
                flexDirection: "column",
                textAlign: "center",
              }}
            >
              <div>{boardName}</div>
            </CardContent>
          </CardActionArea>
        </Link>
        <div style={{ margin: 12, display: "flex" }}>
          <Delete
            ID={boardId}
            eleName={eleName}
            initialData={initialData}
            allData={allData}
          />
        </div>
      </Card>
    </>
  );
};

export default DisplayBoard;
