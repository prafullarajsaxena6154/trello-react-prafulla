import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import GetCheckLists from "../ChecklistManipulation/GetCheckLists";
import { Mode } from "../Theme";
import { useContext } from "react";
export default function CardData({ card }) {
  const { darkMode } = useContext(Mode);
  const [open, setOpen] = React.useState(false);
  const [scroll, setScroll] = React.useState("paper");

  const handleClickOpen = (scrollType) => () => {
    setOpen(true);
    setScroll(scrollType);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);

  return (
    <div>
      <Button sx={{ height: 2 }} onClick={handleClickOpen("paper")}>
        {card.name}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        scroll={scroll}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle style={{backgroundColor:!darkMode?"white":"black", color:darkMode?"white":"black"}} id="scroll-dialog-title">
          {card.name}
          <Button onClick={handleClose}>Cancel</Button>
        </DialogTitle>
        <DialogContent style={{backgroundColor:!darkMode?"white":"black", color:darkMode?"white":"black"}} dividers={scroll === "paper"}>
          <DialogContentText
            id="scroll-dialog-description"
            ref={descriptionElementRef}
            tabIndex={-1}
          >
            <GetCheckLists card={card} />
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}
