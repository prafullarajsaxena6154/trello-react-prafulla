import {
  Button,
  Alert,
  Card,
  CardActionArea,
  CardContent,
  TextField,
} from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import { useLoginContext } from "../LoginDetails";

const CreateNewCard = ({ listID, updatedCards }) => {
  const { apiToken, apiKey } = useLoginContext();
  const [cardName, setCardName] = useState("");
  const [isAddCardClicked, setIsAddCardClicked] = React.useState(false);
  const [error, setError] = useState("");
  function createCard() {
    if (cardName != "") {
      axios
        .post(
          `https://api.trello.com/1/cards?name=${cardName}&idList=${listID}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => {
          updatedCards(response.data);
          setError("");
          setCardName("");
        })
        .catch((err) => {
          setError("An error occurred. Please try again.");
        });
    } else {
      setError("Provide a card name!");
    }
  }
  return (
    <div style={{ marginTop: "1rem" }}>
      {error && <Alert severity="error">{error}</Alert>}
      {!isAddCardClicked ? (
        <Card sx={{ marginTop: 3 }}>
          <CardActionArea onClick={() => setIsAddCardClicked(true)}>
            <CardContent>Add Card</CardContent>
          </CardActionArea>
        </Card>
      ) : (
        <div style={{ marginTop: "1rem" }}>
          <TextField
            id="filled-basic"
            label="List Title"
            variant="filled"
            value={cardName}
            onChange={(event) => setCardName(event.target.value)}
            error={Boolean(error)}
          />
          <div>
            <Button onClick={createCard}>CREATE</Button>
            <Button
              onClick={() => {
                setIsAddCardClicked(false);
                setError("");
              }}
            >
              Cancel
            </Button>
          </div>
        </div>
      )}
    </div>
  );
};

export default CreateNewCard;
