import * as React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";
import { styled } from "@mui/material/styles";
import CardData from "./CardData";
import { CardActionArea } from "@mui/material";
import { Mode } from "../Theme";
import { useContext } from "react";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export default function DisplayCards({ card }) {
  const { darkMode } = useContext(Mode);
  return (
    <Box sx={{ width: "100%", marginBottom: 1}}>
      <Stack spacing={2}>
        <CardActionArea>
          <Item sx={{ display: "flex", justifyContent: "space-between", bgcolor: !darkMode?"white":"black"}}>
            <div style={{ display: "flex", alignItems: "center" }}>
              <CardData card={card} />
            </div>
          </Item>
        </CardActionArea>
      </Stack>
    </Box>
  );
}
