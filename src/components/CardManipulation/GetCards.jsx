import React, { useState } from "react";
import DisplayCards from "./DisplayCards";
import { useEffect } from "react";
import axios from "axios";
import CreateNewCard from "./CreateNewCard";
import Delete from "../Delete";
import { set_Card, create_Card } from "../Redux/CardSlice";
import { useDispatch, useSelector } from "react-redux";
import { Alert, Snackbar } from "@mui/material";
import { useLoginContext } from "../LoginDetails";

const GetCards = ({ listID }) => {
  const { apiToken, apiKey} = useLoginContext();
  const [errorSnackbarOpen, setErrorSnackbarOpen] = useState(false);
  let path = `https://api.trello.com/1/lists/${listID}/cards?key=${apiKey}&token=${apiToken}`;

  const dispatch = useDispatch();
  useEffect(() => {
    axios
      .get(path)
      .then((data) => {
        dispatch(set_Card({ id: listID, data: data.data }));
      })
      .catch((err) => {
        setErrorSnackbarOpen(true);
      });
  }, []);
  const cardData = useSelector((state) => state.card.card[listID] || []);
  const handleNewCardCreated = (newCardData) => {
    dispatch(create_Card({ id: listID, data: newCardData }));
  };
  function handleSnackbarClose() {
    setErrorSnackbarOpen(false);
  }

  return (
    <div>
      {cardData &&
        cardData.map((ele) => (
          <div
            key={ele.id}
            style={{ display: "flex", gap: 25, alignItems: "start" }}
          >
            <DisplayCards key={ele.id} card={ele} />
            <Delete
              eleName={"Card"}
              ID={ele.id}
              initialData={cardData}
              allData={(data) => dispatch(set_Card({ id: listID, data: data }))}
            />
          </div>
        ))}

      <CreateNewCard
        key={listID}
        listID={listID}
        updatedCards={handleNewCardCreated}
      />
      <Snackbar
        open={errorSnackbarOpen}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
      >
        <Alert
          elevation={6}
          variant="filled"
          onClose={handleSnackbarClose}
          severity="error"
        >
          An error occurred while fetching card data.
        </Alert>
      </Snackbar>
    </div>
  );
};
export default GetCards;
