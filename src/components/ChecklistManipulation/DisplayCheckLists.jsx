import React from "react";
import GetCheckListItems from "../ItemManipulation/GetCheckListItems";
import Delete from "../Delete";
import { Mode } from "../Theme";
import { useContext } from "react";
const DisplayCheckLists = ({ checkList, initialData , updatedCheckLists }) => {
  const {darkMode} = useContext(Mode);
  return (
    <>
      <h4 style={{color:darkMode?"white":"black"}}>{checkList.name}</h4>
      <Delete
        ID={checkList.id}
        eleName={"CheckList"}
        initialData = {initialData}
        allData={(data) => updatedCheckLists(data)}
      />
      <GetCheckListItems checkList={checkList} />
    </>
  );
};

export default DisplayCheckLists;
