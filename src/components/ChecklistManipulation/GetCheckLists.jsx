import axios from "axios";
import React, { useEffect, useState } from "react";
import DisplayCheckLists from "./DisplayCheckLists";
import { Alert, Button, Snackbar, TextField } from "@mui/material";
import { set_Checklist,create_Checklist } from "../Redux/CheckListSlice";
import { useDispatch,useSelector } from "react-redux";
import { Mode } from "../Theme";
import { useContext } from "react";
import { useLoginContext } from "../LoginDetails";
const GetCheckLists = ({ card }) => {
  const { darkMode } = useContext(Mode);
  const { apiToken, apiKey} = useLoginContext();
  const dispatch=useDispatch();
  const [newCheckListName, setNewCheckListName] = React.useState("");
  const [error, setError] = useState("");
  const [errorSnackbarOpen, setErrorSnackbarOpen] = useState(false);
  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/cards/${card.id}/checklists?key=${apiKey}&token=${apiToken}`
      )
      .then((data) => {
        dispatch(set_Checklist(data.data));
      }).catch((err) => {
        setErrorSnackbarOpen(true);
      });
  }, []);
const checkListData=useSelector((state) => state.checklist.checklist)
  function handleCreateCheckList() {
    if (newCheckListName != "") {
      axios
        .post(
          `https://api.trello.com/1/checklists?name=${newCheckListName}&idCard=${card.id}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => {
          dispatch(create_Checklist(response.data));
          setNewCheckListName("");
          setError("");
        })
        .catch((err) => {
          setError("An error occurred. Please try again.");
        });
    } else {
      setError("Please enter Checklist Name!");
    }
  }
  function handleSnackbarClose() {
    setErrorSnackbarOpen(false);
  }
  return (
    <>
      <div style={{color:darkMode?"white":"black"}}>Create New CheckList</div>
      <TextField
        id="filled-basic"
        label="Checklist Title"
        variant="filled"
        value={newCheckListName}
        onChange={(event) => setNewCheckListName(event.target.value)}
      />
      {error && <Alert severity="error">{error}</Alert>}
      <Button onClick={handleCreateCheckList} variant="contained">
        Create
      </Button>
      {checkListData.map((ele) => {
        return (
          <div>
            <DisplayCheckLists
              key={ele.id}
              initialData={checkListData}
              checkList={ele}
              updatedCheckLists={(data) => dispatch(set_Checklist(data))}
            />
          </div>
        );
      })}
      <Snackbar
        open={errorSnackbarOpen}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
      >
        <Alert
          elevation={6}
          variant="filled"
          onClose={handleSnackbarClose}
          severity="error"
        >
          An error occurred while fetching checklist data.
        </Alert>
      </Snackbar>
    </>
  );
};

export default GetCheckLists;
