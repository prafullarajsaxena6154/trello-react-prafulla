import * as React from "react";
import PropTypes from "prop-types";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import axios from "axios";
import { Alert } from "@mui/material";
import { useLoginContext } from "./LoginDetails";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export default function Delete({
  ID,
  eleName,
  initialData,
  allData,
  preEleID,
}) {
  const [open, setOpen] = React.useState(false);
  const [error, setError] = React.useState("");
  const { apiToken, apiKey} = useLoginContext();

  const handleClickOpen = () => {
    setOpen(true);
    setError('');
  };
  const handleClose = () => {
    console.log(error);
    setOpen(false);
  };
  const handleDelete = () => {
    if (eleName === "Board") {
      axios
        .delete(
          `https://api.trello.com/1/boards/${ID}?key=${apiKey}&token=${apiToken}`
        )
        .then((response) => {
          initialData = initialData.filter((ele) => ele.id != ID);
          allData(initialData);
          setError('');
        })
        .catch((err) => {setError("Error occured in deleting!")});
    } else if (eleName === "Card") {
      axios
        .delete(
          `https://api.trello.com/1/cards/${ID}?key=${apiKey}&token=${apiToken}`
        )
        .then(() => {
          initialData = initialData.filter((ele) => ele.id != ID);
          allData(initialData);
          setError('');
        })
        .catch((err) => setError("Error occured in deleting!"));
    } else if (eleName === "List") {
      axios
        .put(
          `https://api.trello.com/1/lists/${ID}?closed=true&key=${apiKey}&token=${apiToken}`
        )
        .then(() => {
          initialData = initialData.filter((ele) => ele.id != ID);
          allData(initialData);
          setError('');
        })
        .catch((err) => setError("Error occured in deleting!"));
    } else if (eleName === "CheckList") {
      axios
        .delete(
          `https://api.trello.com/1/checklists/${ID}?key=${apiKey}&token=${apiToken}`
        )
        .then(() => {
          initialData = initialData.filter((ele) => ele.id != ID);
          allData(initialData);
          setError('');
        })
        .catch((err) => setError("Error occured in deleting!"));
    } else if (eleName === "Checklist Item") {
      axios
        .delete(
          `https://api.trello.com/1/checklists/${preEleID}/checkItems/${ID}?key=${apiKey}&token=${apiToken}`
        )
        .then(() => {
          initialData = initialData.filter((ele) => ele.id != ID);
          allData(initialData);
          setError('');
        })
        .catch((err) => {
          setError("Error occured in deleting!");
        });
    }
    error === "" ? setOpen(true) : setOpen(false);
  };

  return (
    <div>
      <Button
        variant="outlined"
        sx={{ color: "red" }}
        onClick={handleClickOpen}
      >
        <i className="fa-solid fa-trash"></i>
      </Button>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Confirm Deletion
        </BootstrapDialogTitle>
        <DialogContent dividers>
          {error && <Alert severity="error">{error}</Alert>}
          <Typography gutterBottom>
            Are you sure you want to delete this {eleName}?
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleDelete}>
            YES, DELETE.
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
}
