import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Switch from "@mui/material/Switch";
import { Link } from "react-router-dom";
import { Mode } from "./Theme";
import { useContext } from "react";
import "../App.css";
export default function ButtonAppBar() {
  const { darkMode, setDarkMode } = useContext(Mode);

  function handleDarkMode() {
    setDarkMode(!darkMode);
  }

  return (
    <div>
      <Box>
        <AppBar
          style={{ background: "#85B8FF", color: "white" }}
          position="static"
        >
          <Toolbar
            sx={{
              bgcolor: !darkMode ? "white" : "black",
              justifyContent: "space-between",
            }}
            className="dark"
          >
            <Link to="/">
              <h4 style={{ color: darkMode ? "white" : "black" }}>Home</h4>
            </Link>
            <Typography
              sx={{ color: darkMode ? "white" : "black" }}
              variant="h4"
              component="div"
            >
              <div className="trello" style={{paddingLeft:'5rem'}}>
                Trello <i className="fa-brands fa-trello"></i>
              </div>
            </Typography>
            <div style={{ color: darkMode ? "white" : "black" }}>
              Light
              <Switch
                type="checkbox"
                checked={darkMode}
                onChange={handleDarkMode}
              />
              Dark
            </div>
          </Toolbar>
        </AppBar>
      </Box>
    </div>
  );
}
