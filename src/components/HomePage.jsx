import React from "react";
import { useEffect, useState } from "react";
import DisplayBoard from "./BoardManipulation/DisplayBoard";
import CreateNewBoard from "./BoardManipulation/CreateNewBoard";
import axios from "axios";
import Loader from "../../Loader";
import { useDispatch, useSelector } from "react-redux";
import { set_Board, create_Board } from "./Redux/BoardSlice";
import { Alert, Snackbar } from "@mui/material";
import Login from "./Login";
import { useLoginContext } from "./LoginDetails";
const HomePage = () => {
  const dispatch = useDispatch();
  const { apiToken, apiKey } = useLoginContext();
  const boardData = useSelector((state) => state.board.board);
  const [loading, setLoading] = useState(true);
  const [loggedIn, setLogin] = useState(false);
  
    useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`
      )
      .then((data) => {
        dispatch(set_Board(data.data));
        setLoading(false);
      })
  }, [loggedIn]);
  function handleUpdatedBoardsData(data) {
    dispatch(create_Board(data));
  }
  let homePageData = loading ? (
    <Loader />
  ) : boardData.length === 0 ? (
    <CreateNewBoard
      noOfBoards={boardData.length}
      boards={handleUpdatedBoardsData}
    />
  ) : (
    <>
      {boardData.map((ele) => {
        return (
          <DisplayBoard
            key={ele.id}
            board={ele}
            initialData={boardData}
            allData={(data) => dispatch(set_Board(data))}
          />
        );
      })}
      <CreateNewBoard
        noOfBoards={boardData.length}
        boards={handleUpdatedBoardsData}
      />
    </>
  );
  return (loggedIn?
    <>
      {homePageData}
    </>
  : <>
    <Login logIn = {() => setLogin(true)}/>
  </>);
};

export default HomePage;
