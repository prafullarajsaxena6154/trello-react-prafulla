import { Alert, Button, TextField } from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import { useLoginContext } from "../LoginDetails";
const CreateNewCheckListItem = ({ checkList, checkListItemData }) => {
  const [checkListItemName, setCheckListsItemsName] = useState("");
  const { apiToken, setApiToken, apiKey, setApiKey } = useLoginContext();
  const [error, setError] = useState("");
  function handleCreateCheckListItem() {
    if (checkListItemName != "") {
      axios
        .post(
          `https://api.trello.com/1/checklists/${checkList.id}/checkItems?name=${checkListItemName}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => {
          checkListItemData(response.data);
          setCheckListsItemsName("");
          setError("");
        })
        .catch((err) => {
          setError("An error occurred. Please try again.");
        });
    } else {
      setError("Please Enter Item Name!");
    }
  }
  return (
    <>
      <TextField
        id="filled-basic"
        label="Checklist Item Title"
        variant="filled"
        value={checkListItemName}
        onChange={(event) => setCheckListsItemsName(event.target.value)}
      />
      {error && <Alert severity="error">{error}</Alert>}
      <Button variant="contained" onClick={handleCreateCheckListItem}>
        Create
      </Button>
    </>
  );
};

export default CreateNewCheckListItem;
