import { Checkbox } from "@mui/material";
import React from "react";
import Delete from "../Delete";
import axios from "axios";
import { Mode } from "../Theme";
import { useContext } from "react";
import { useLoginContext } from "../LoginDetails";
const label = { inputProps: { "aria-label": "Checkbox demo" } };

const DisplayCheckListItems = ({
  checkListItem,
  initialData,
  checkList,
  checkListItemData,
}) => {
  const { apiToken, setApiToken, apiKey, setApiKey } = useLoginContext();
  function changeCheckState() {
    const updatedState =
      checkListItem.state === "incomplete" ? "complete" : "incomplete";
    const updatedCheckListItem = { ...checkListItem, state: updatedState };
    axios
      .put(
        `https://api.trello.com/1/cards/${checkList.idCard}/checkItem/${checkListItem.id}?state=${updatedState}&key=${apiKey}&token=${apiToken}`
      )
      .then(() => {
        initialData = initialData.map((ele) => {
          return ele.id === checkListItem.id ? updatedCheckListItem : ele;
        });
        checkListItemData(initialData);
      });
  }
  const {darkMode} = useContext(Mode);
  return (
    <div style={{color:darkMode?"white":"black"}}>
      <Checkbox
        {...label}
        sx={{color:"red"}}
        checked={checkListItem.state !== "incomplete"}
        onChange={changeCheckState}
      />
      {checkListItem.name}
      <Delete
        ID={checkListItem.id}
        initialData={initialData}
        eleName={"Checklist Item"}
        allData={(data) => checkListItemData(data)}
        preEleID={checkList.id}
      />
    </div>
  );
};

export default DisplayCheckListItems;
