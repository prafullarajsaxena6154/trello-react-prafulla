import axios from "axios";
import React, { useEffect, useState } from "react";
import LinearWithValueLabel from "./LinearWithValueLabel";
import DisplayCheckListItems from "./DisplayCheckListItems";
import CreateNewCheckListItem from "./CreateNewCheckListItem";
import { useDispatch,useSelector } from "react-redux";
import { set_Item,create_Item } from "../Redux/CheckItemSlice";
import { Alert, Snackbar } from "@mui/material";
import { Mode } from "../Theme";
import { useContext } from "react";
import { useLoginContext } from "../LoginDetails";
const GetCheckListItems = ({ checkList }) => {
  const { darkMode } = useContext(Mode);
  const { apiToken, setApiToken, apiKey, setApiKey } = useLoginContext();
  const dispatch=useDispatch();
  const [errorSnackbarOpen, setErrorSnackbarOpen] = useState(false);
  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/checklists/${checkList.id}/checkItems?key=${apiKey}&token=${apiToken}`
      )
      .then((data) => {
        // console.log(checkList.id);
        dispatch(set_Item({id:checkList.id,data:data.data}));
      })
      .catch((err) => {
        setErrorSnackbarOpen(true);
      });
  }, []);
  const itemData=useSelector((state) => state.checkitem.checkitem[checkList.id]||[]);
  // console.log(itemData);
  let completionValue = 0;
  if (itemData.length != 0) {
    let checkedItems = 0;
    itemData.forEach((ele) => {
      if (ele.state === "complete") {
        checkedItems += 1;
      }
    });
    completionValue = (checkedItems / itemData.length) * 100;
  }
  function handleSnackbarClose() {
    setErrorSnackbarOpen(false);
  }
  return (
    <>
      <LinearWithValueLabel value={completionValue} />
      {itemData && itemData.map((ele) => {
        return (
          <DisplayCheckListItems
            key={ele.id}
            initialData={itemData}
            checkListItem={ele}
            checkList={checkList}
            checkListItemData={(data) => dispatch(set_Item({id:checkList.id,data:data}))}
          />
        );
      })}
      <div>
        <div style={{color:darkMode?"white":"black"}}>Create Item</div>
        <CreateNewCheckListItem
          checkList={checkList}
          checkListItemData={(data) => dispatch(create_Item({id:checkList.id,data:data}))}
        />
      </div>
      <Snackbar
        open={errorSnackbarOpen}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
      >
        <Alert
          elevation={6}
          variant="filled"
          onClose={handleSnackbarClose}
          severity="error"
        >
          An error occurred while fetching items data.
        </Alert>
      </Snackbar>
    </>
  );
};

export default GetCheckListItems;
