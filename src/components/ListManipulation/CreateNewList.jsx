import {
  Alert,
  Button,
  Card,
  CardActionArea,
  CardContent,
  TextField,
} from "@mui/material";
import axios from "axios";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Mode } from "../Theme";
import { useContext } from "react";
import { useLoginContext } from "../LoginDetails";

const CreateNewList = ({ allListsData }) => {
  const { apiToken, setApiToken, apiKey, setApiKey } = useLoginContext();
  const { darkMode } = useContext(Mode);
  const { id } = useParams();
  const [listName, setListName] = useState("");
  const [error, setError] = useState("");
  function handleListChange() {
    if (listName != "") {
      axios
        .post(
          `https://api.trello.com/1/lists?name=${listName}&idBoard=${id}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => {
          response = response.data;
          setError("");
          setListName("");
          return response;
        })
        .then((data) => {
          allListsData(data);
        })
        .catch((err) => {
          setError("An error occurred. Please try again.");
        });
    } else {
      setError("Please Enter list name!");
    }
  }
  return (
    <div>
      <Card sx={{ minWidth: 275 }} className="dark">
        <CardActionArea>
          <CardContent className={darkMode ? "dark" : "null"}>
            <div>
              Create New List
            </div>
            <TextField
              id="filled-basic"
              label="List Title"
              variant="filled"
              value={listName}
              sx={{ backgroundColor: "white", marginBottom: 1 }}
              onChange={(event) => setListName(event.target.value)}
            />
            {error && <Alert severity="error">{error}</Alert>}
            <Button onClick={handleListChange} variant="contained">
              Create
            </Button>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
};

export default CreateNewList;
