import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import GetCards from "../CardManipulation/GetCards";
import Delete from "../Delete";
import { Mode } from "../Theme";
import { useContext } from "react";

export default function DisplayList({ list, initialData, allListsData }) {
  let eleName = "List";
  const { darkMode } = useContext(Mode);
  return (
    <Card
      key={list.id}
      sx={{
        minWidth: 275,
        backgroundColor: "rgba(255, 255, 255, 0.85)",
        marginBottom: "auto",
        color: "black",
      }}
    >
      <CardContent sx={{ bgcolor: !darkMode ? "white" : "black" }}>
        <Typography
          sx={{
            fontSize: 14,
            display: "flex",
            justifyContent: "space-between",
            marginBottom: 3,
            color: darkMode ? "white" : "black",
          }}
          gutterBottom
        >
          <div>
            {list.name}
          </div>
          <Delete
            eleName={eleName}
            ID={list.id}
            initialData={initialData}
            allData={(data) => allListsData(data)}
          />
        </Typography>
        <GetCards listID={list.id} />
      </CardContent>
    </Card>
  );
}
