import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import DisplayList from "./DisplayList";
import CreateNewList from "./CreateNewList";
import { set_List, create_List } from "../Redux/ListSlice";
import { useDispatch, useSelector } from "react-redux";
import { Alert, Snackbar } from "@mui/material";
import { useLoginContext } from "../LoginDetails";
const GetLists = () => {
  const { id } = useParams();
  const { apiToken, apiKey } = useLoginContext();
  const [errorSnackbarOpen, setErrorSnackbarOpen] = useState(false);
  let path = `https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${apiToken}`;
  const dispatch = useDispatch();

  useEffect(() => {
    axios
      .get(path)
      .then((data) => {
        dispatch(set_List(data.data));
      })
      .catch((err) => {
        setErrorSnackbarOpen(true);
      });
  }, []);
  function handleSnackbarClose() {
    setErrorSnackbarOpen(false);
  }
  const listData = useSelector((state) => state.list.list);
  return (
    <div
      style={{
        display: "flex",
        gap: 50,
        overflowX: "auto",
        height: "auto",
        paddingBottom: 20,
        backgroundColor: "rgba(0, 0, 0, 0.3)",
        borderRadius: 5,
      }}
    >
      {listData.map((ele) => {
        return (
          <div key={ele.id} style={{ padding: "0.8rem", minHeight: "79vh" }}>
            <DisplayList
              key={ele.id}
              initialData={listData}
              list={ele}
              allListsData={(data) => dispatch(set_List(data))}
            />
          </div>
        );
      })}
      <div style={{ padding: "0.8rem" }}>
        <CreateNewList allListsData={(data) => dispatch(create_List(data))} />
      </div>
      <Snackbar
        open={errorSnackbarOpen}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
      >
        <Alert
          elevation={6}
          variant="filled"
          onClose={handleSnackbarClose}
          severity="error"
        >
          An error occurred while fetching list data.
        </Alert>
      </Snackbar>
    </div>
  );
};

export default GetLists;
