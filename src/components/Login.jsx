import { Card, TextField, Button, Snackbar, Alert } from "@mui/material";
import React, { useState } from "react";
import { Mode } from "./Theme";
import { useLoginContext } from "./LoginDetails";
import { useContext } from "react";
import axios from "axios";

const Login = ({ logIn }) => {
  const { darkMode } = useContext(Mode);
  const { apiToken, setApiToken, apiKey, setApiKey } = useLoginContext();
  const [errorSnackbarOpen, setErrorSnackbarOpen] = useState(false);
  function handleLogin() {
    console.log(apiToken);
    console.log(apiKey);
    axios
      .get(
        `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`
      )
      .then(() => {
        logIn();
      })
      .catch(() => {
        setErrorSnackbarOpen(true);
      });
  }
  function handleSnackbarClose() {
    setErrorSnackbarOpen(false);
  }
  return (
    <>
      <Card
        sx={{
          color: darkMode ? "white" : "black",
          bgcolor: !darkMode ? "white" : "black",
          display: "flex",
          flexDirection: "column",
          gap: "1rem",
          padding: "1rem",
        }}
      >
        <h1>Login Into Your Trello Account</h1>
        <h5>
          Test-Token:
          ATTA8ed8cbe5e33aabb2a23273398f67e4020cfd0f3081b5d20d9912b849b6e98cbdFFA277BC
          <br />
          Test-Key: 41aa09a04c866de892a803359f392787
        </h5>
        <TextField
          id="outlined-basic"
          label="API-Key"
          variant="outlined"
          value={apiKey}
          onChange={(e) => setApiKey(e.target.value)}
        />
        <TextField
          id="outlined-basic"
          label="API-Token"
          variant="outlined"
          value={apiToken}
          onChange={(e) => setApiToken(e.target.value)}
        />
        <i>*if you dont have your token and key, visit the official Trello website and create one.</i>
        <i>*use the Test-key and Test-token for Guest profile.</i>
        <Button onClick={handleLogin} variant="contained">
          Log-In
        </Button>
      </Card>
      <Snackbar
        open={errorSnackbarOpen}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
      >
        <Alert
          elevation={6}
          variant="filled"
          onClose={handleSnackbarClose}
          severity="error"
        >
          Wrong API-Token or API-Key.
        </Alert>
      </Snackbar>
    </>
  );
};

export default Login;
