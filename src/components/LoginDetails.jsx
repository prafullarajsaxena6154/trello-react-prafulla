import React, { createContext, useContext, useState } from "react";

const LoginDetails = createContext();

export function useLoginContext() {
  return useContext(LoginDetails);
}

export function LoginProvider({ children }) {
  const [apiToken, setApiToken] = useState("");
  const [apiKey, setApiKey] = useState("");

  return (
    <LoginDetails.Provider value={{ apiToken, setApiToken, apiKey, setApiKey }}>
      {children}
    </LoginDetails.Provider>
  );
}
