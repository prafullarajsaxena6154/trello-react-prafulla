import { createSlice } from "@reduxjs/toolkit";
const BoardSlice = createSlice({
    name:'board',
    initialState: {board:[]},
    reducers:{
        set_Board(state, action){
            state.board=[...action.payload];
        },
        create_Board(state, action){
            state.board.push(action.payload)
        }
    }
})
export const {set_Board, create_Board} = BoardSlice.actions;
export default BoardSlice;