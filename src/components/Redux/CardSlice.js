import { createSlice } from "@reduxjs/toolkit";
const CardSlice = createSlice({
    name:'card',
    initialState: {card:{}},
    reducers:{
        set_Card(state, action){
            
            const cardObj={
                id:action.payload.id,
                data:action.payload.data
            };
            state.card[cardObj.id] = [...cardObj.data];
            
        },
        create_Card(state, action){
            const { id, data } = action.payload;
      state.card = {
        ...state.card,
        [id]: [...(state.card[id] || []), data]
        }
    }
    }
})
export const {set_Card, create_Card} = CardSlice.actions;
export default CardSlice;