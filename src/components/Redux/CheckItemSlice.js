import { createSlice } from "@reduxjs/toolkit";
const CheckItemSlice = createSlice({
    name:'checkitem',
    initialState: {checkitem:{}},
    reducers:{
        set_Item(state, action){
            
            const itemObj={
                id:action.payload.id,
                data:action.payload.data
            };
            state.checkitem[itemObj.id] = [...itemObj.data];
            
        },
        create_Item(state, action){
            const { id, data } = action.payload;
      state.checkitem = {
        ...state.checkitem,
        [id]: [...(state.checkitem[id] || []), data]
        }
        
    }
    }
})
export const {set_Item, create_Item} = CheckItemSlice.actions;
export default CheckItemSlice;