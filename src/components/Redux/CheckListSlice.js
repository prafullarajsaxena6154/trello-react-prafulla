import { createSlice } from "@reduxjs/toolkit";
const CheckListSlice = createSlice({
    name:'checklist',
    initialState: {checklist:[]},
    reducers:{
        set_Checklist(state, action){
            state.checklist=[...action.payload];
        },
        create_Checklist(state, action){
            state.checklist.push(action.payload)
        }
    }
})
export const {set_Checklist, create_Checklist} = CheckListSlice.actions;
export default CheckListSlice;