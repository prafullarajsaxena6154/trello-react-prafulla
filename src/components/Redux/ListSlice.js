import { createSlice } from "@reduxjs/toolkit";

const ListSlice = createSlice({
    name:'list',
    initialState: {list:[]},
    reducers:{
        set_List(state, action){
            state.list=[...action.payload];
        },
        create_List(state, action){
            state.list.push(action.payload)
        }
    }
})
export const {set_List, create_List} = ListSlice.actions;
export default ListSlice;