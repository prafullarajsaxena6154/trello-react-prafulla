import { configureStore } from '@reduxjs/toolkit'
import BoardSlice from './BoardSlice'
import ListSlice from './ListSlice'
import CardSlice from './CardSlice'
import CheckListSlice from './CheckListSlice'
import CheckItemSlice from './CheckItemSlice'

const store = configureStore({
  reducer: {
    board: BoardSlice.reducer,
    list: ListSlice.reducer,
    card: CardSlice.reducer,
    checklist: CheckListSlice.reducer,
    checkitem: CheckItemSlice.reducer
  },
})
export default store