import { useState, createContext } from "react";

const Mode = createContext();

const ModeProvider = ({ children }) => {
  const [darkMode, setDarkMode] = useState(false);

  return (
    <Mode.Provider value={{ darkMode, setDarkMode }}>{children}</Mode.Provider>
  );
};

export { Mode, ModeProvider };
