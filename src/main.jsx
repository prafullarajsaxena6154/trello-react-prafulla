import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import { BrowserRouter } from "react-router-dom";
import { ModeProvider } from "./components/Theme.jsx";
import store from "./components/Redux/Store.js";
import { Provider } from "react-redux";
import { LoginProvider } from "./components/LoginDetails.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
  <Provider store={store}>
    <ModeProvider>
      <LoginProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </LoginProvider>
    </ModeProvider>
  </Provider>
);
